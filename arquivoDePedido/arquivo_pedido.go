package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Header  Header    `json:"Header"`
	Detalhe []Detalhe `json:"Detalhe"`
	Trailer Trailer   `json:"Trailer"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		var index int32
		if identificador == "1" {
			err = arquivo.Header.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.Detalhe[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "3" {
			err = arquivo.Trailer.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
