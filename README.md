## Arquivo de Pedido
gerador-layouts arquivoDePedido header TipoRegistro:int32:0:1 CodigoCliente:string:1:16 NumeroPedido:string:16:28 DataPedido:int32:28:36 TipoCompra:string:36:37 TipoRetorno:string:37:38 ApontadorCondicaoComercial:int32:38:43 NumeroPedidoCliente:string:43:58

gerador-layouts arquivoDePedido detalhe TipoRegistro:int32:0:1 NumeroPedido:string:1:13 CodigoPedido:int64:13:26 Quantidade:int32:26:31 Desconto:float32:31:36:2 Prazo:int32:36:39 UtilizacaoDesconto:string:39:40 UtilizacaoPrazo:string:40:41

gerador-layouts arquivoDePedido trailer TipoRegsitro:int32:0:1 NumeroPedido:string:1:13 QuantidadeItens:int32:13:18 QuantidadeUnidades:int64:18:28

## Arquivo de Retorno
gerador-layouts retornoDePedido header TipoRegistro:int32:0:1 CnpjFarmacia:string:1:16 NumeroPedidoIndustria:string:1:28 DataProcessamento:int32:28:36 HoraProcessamento:int32:36:44 NumeroPedidoOL:string:44:56 CodigoRetorno:int32:56:58

gerador-layouts retornoDePedido detalhe TipoRegistro:int32:0:1 CodigoProduto:int64:1:14 NumeroPedido:string:14:26 CondicaoPagamento:string:26:27 QuantidadeAtendida:int32:27:32 DescontoAplicado:float32:32:37:2 PrazoConcedido:int32:37:40 QuantidadeNaoAtendida:int32:40:45 Motivo:string:45:95

gerador-layouts retornoDePedido trailer TipoRegsitro:int32:0:1 NumeroPedido:int32:1:13 QuantidadeLinhas:int32:13:18 QuantidadeItensAtendidos:int32:18:23 QuantidadeItensNaoAtendida:int32:23:28